//load the libraries
var path = require("path");
var express = require("express");

//create an instance 
var app = express();

//define the routes
var createUser = function(user) {
    return ({
        email:user.email,
        password:user.password,
        name:user.name,
        gender:user.gender,
        dateOfBirth: user.dateOfBirth,
        address: user.address,
        country: user.country,
        contact: user.contact
    });

}
app.use("/libs", express.static(path.join(__dirname, "bower_components")));
app.use(express.static(path.join(__dirname, "public")));
app.get("/reg_user", function(req, res) {
    newUser.push(createUser(req.query));
    console.log("User: %s", JSON.stringify(newUser));
    res.status(200).end();
})
//setup the server
app.set("port", process.env.APP_PORT || 3000);
app.listen(app.get("port"), function() {
    console.log("Application started at %s on port %d", new Date(), app.get("port"));
})