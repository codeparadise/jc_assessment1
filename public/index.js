//registration

(function() {
    var RegApp = angular.module("RegApp", []); //adding ngAnimate as a dependency for RegApp
    /*
    var RegCtrl = function() {
        var firstCtrl = this;

        firstCtrl.email = "wow,angular pass this here"

    }*/
    var initForm = function(ctrl) {
        ctrl.email = "",
        ctrl.password = "",
        ctrl.name = "",
        ctrl.gender = "",
        ctrl.dateOfBirth = "",
        ctrl.address = "",
        ctrl.country = "",
        ctrl.contact = ""
    }
    var createQueryString = function(ctrl) {
        return({
            email: ctrl.email,
            password: ctrl.password,
            gender: ctrl.gender,
            dateOfBirth: ctrl.dateOfBirth,
            address: ctrl.address,
            country: ctrl.country,
            contactNum: ctrl.contactNum
    })
        
    }

    function RegCtrl($http) {
        var regCtrl = this;
        initForm(regCtrl);
    
        regCtrl.validateAge = function () {
            var date = new Date(regCtrl.dateOfBirth);
            date.setFullYear(date.getFullYear() + 18);
            return date < new Date();
        };

        regCtrl.register = function() {
            $http.get("/reg_user", {
                params: createQueryString(regCtrl)
            })
            .then(function() {
                    window.location = "/success.html"
                })
                .catch(function() {
                    alert("Oops! There seems to be some problem in registration. Please try again.");
                });
            
            initForm(regCtrl);
        }
            
    };
    // !!!!validate dateofBirth not working!!!!
  /*
    RegCtrl.validateAge = function() {
            
            var date = new Date(firstCtrl.reg.dateOfBirth);
            date.setFullYear(date.getFullYear() + 18);
           
            return date < new Date();
           
        };
    */
        console.log(">>in function");
        //console.log(">>date of birth %s", JSON.stringify(firstCtrl.result));
        


    

    //for
    RegApp.controller("RegCtrl", ["$http", RegCtrl]);
    

})(); 